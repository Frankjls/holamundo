using libHolaMundo;


namespace TestHolaMundo
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            //arrange
            Mensaje mimensaje = new Mensaje();
            string valorReal, valosEsperado = "Hola Mundo";

            //act
            mimensaje.Saludo = "Hola Mundo";
            valorReal = mimensaje.Saludar();

            //Assert
            Assert.Equal(valorReal, valosEsperado);
        }
    }
}