﻿namespace libHolaMundo
{
    public class Mensaje
    {
        private string _Saludo;

        public string Saludo { get => _Saludo; set => _Saludo = value; }

        public string Saludar()
        {
            return _Saludo;
        }
    }
}